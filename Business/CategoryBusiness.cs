﻿using Business.Models;
using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class CategoryBusiness
    {
        #region Constructors
        public CategoryBusiness()
        {
            repository = new DataRepository<Category>();
        }
        #endregion

        #region Properties
        private IDataRepository<Category> repository;
        #endregion

        #region Methods
        public CategoryModel Get(int id)
        {
            Category entity = repository.GetById(id);
            return new CategoryModel(entity);
        }

        public IEnumerable<CategoryModel> GetAll()
        {
            foreach (Category entity in repository.GetAll())
            {
                yield return new CategoryModel(entity);
            }
        }

        public int Insert(CategoryModel category)
        {
            return repository.Insert(category.ToEntity()).Id;
        }

        public void Update(CategoryModel category)
        {
            repository.Update(category.ToEntity(), category.Id);
        }

        public void Delete(CategoryModel category)
        {
            repository.Delete(category.Id);
        }
        #endregion
    }
}
