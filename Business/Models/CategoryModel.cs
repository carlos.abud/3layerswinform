﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Models
{
    public class CategoryModel
    {
        #region Constructors
        public CategoryModel() { }
        public CategoryModel(Category entity)
        {
            this.Id = entity.Id;
            this.Name = entity.Name;
        }
        #endregion

        #region Properties
        public int Id { get; set; }
        public string Name { get; set; }
        #endregion

        #region Methods
        public Category ToEntity()
        {
            return new Category
            {
                Id = this.Id,
                Name = this.Name
            };
        }
        #endregion
    }
}
