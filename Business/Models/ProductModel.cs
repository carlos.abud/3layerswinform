﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Models
{
    public class ProductModel
    {
        #region Constructors
        public ProductModel() { }

        public ProductModel(Product entity)
        {
            Id = entity.Id;
            Description = entity.Description;
            ExpirationDate = entity.ExpirationDate;
            Stock = entity.Stock;
            Price = entity.Price;
            CategoryId = entity.CategoryId;
        }
        #endregion

        #region Properties
        public int Id { get; set; }
        public string Description { get; set; }
        public DateTime ExpirationDate { get; set; }
        public decimal Stock { get; set; }
        public decimal Price { get; set; }
        public int CategoryId { get; set; }

        private CategoryModel category;
        public CategoryModel Category
        {
            get
            {
                //Con este arreglo nos evitamos, en el constructor, traer todos los objetos de la BD que estén relacionados.
                //Además lo busca sólo si se pide la property. Y si ya se trajo una vez de la BD no vuelve a buscarlo.
                if (category == null)
                {
                    category = new CategoryBusiness().Get(CategoryId);
                }
                return category;
            }
            set => category = value;
        }
        #endregion

        #region Methods
        public Product ToEntity()
        {
            return new Product
            {
                Id = this.Id,
                CategoryId = this.CategoryId,
                Description = this.Description,
                ExpirationDate = this.ExpirationDate,
                Price = this.Price,
                Stock = this.Stock
            };
        }
        #endregion
    }
}
