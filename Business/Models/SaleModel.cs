﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Models
{
    public class SaleModel
    {
        #region Constructors
        public SaleModel() { }
        public SaleModel(Sale entity)
        {
            Id = entity.Id;
            ProductId = entity.ProductId;
            SalePrice = entity.SalePrice;
            StartDate = entity.StartDate;
            EndDate = entity.EndDate;
            Available = entity.Available;
        }
        #endregion

        #region Properties
        public int Id { get; set; }
        public int ProductId { get; set; }
        public decimal SalePrice { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool Available { get; set; }

        private ProductModel product;
        public ProductModel Product
        {
            get
            {
                //Con este arreglo nos evitamos, en el constructor, traer todos los objetos de la BD que estén relacionados.
                //Además lo busca sólo si se pide la property. Y si ya se trajo una vez de la BD no vuelve a buscarlo.
                if (product == null)
                {
                    product = new ProductBusiness().Get(ProductId);
                }
                return product;
            }
            set => product = value;
        }
        #endregion

        #region Methods
        public Sale ToEntity() 
        {
            return new Sale
            {
                Available = this.Available,
                EndDate = this.EndDate,
                Id = this.Id,
                ProductId = this.ProductId,
                SalePrice = this.SalePrice,
                StartDate = this.StartDate
            };
        }
        #endregion
    }
}
