﻿using Business.Models;
using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class ProductBusiness
    {
        #region Constructors
        public ProductBusiness()
        {
            repository = new DataRepository<Product>();
        }
        #endregion

        #region Properties
        private IDataRepository<Product> repository;
        #endregion

        #region Methods
        public ProductModel Get(int id)
        {
            Product entity = repository.GetById(id);
            return new ProductModel(entity);
        }

        public IEnumerable<ProductModel> GetAll()
        {
            foreach (Product entity in repository.GetAll())
            {
                yield return new ProductModel(entity);
            }
        }

        public int Insert(ProductModel product)
        {
            product.Id = repository.Insert(product.ToEntity()).Id;
            new SaleBusiness().CheckProductSale(product);
            return product.Id;
        }

        public void Update(ProductModel product)
        {
            repository.Update(product.ToEntity(), product.Id);
            new SaleBusiness().CheckProductSale(product);
        }

        public void Delete(ProductModel product)
        {
            repository.Delete(product.Id);
            new SaleBusiness().DisableProductSale(product.Id);
        }
        #endregion
    }
}
