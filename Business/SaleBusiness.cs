﻿using Business.Models;
using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class SaleBusiness
    {
        #region Constructors
        public SaleBusiness()
        {
            repository = new DataRepository<Sale>();
        }
        #endregion

        #region Properties
        private IDataRepository<Sale> repository;
        #endregion

        #region Methods
        public SaleModel Get(int id)
        {
            Sale entity = repository.GetById(id);
            return new SaleModel(entity);
        }
        
        public IEnumerable<SaleModel> GetAll()
        {
            foreach (Sale entity in repository.GetAll())
            {
                yield return new SaleModel(entity);
            }
        }

        public int Insert(SaleModel sale)
        {
            return repository.Insert(sale.ToEntity()).Id;
        }

        public void Update(SaleModel sale)
        {
            repository.Update(sale.ToEntity(), sale.Id);
        }

        public void Delete(SaleModel sale)
        {
            repository.Delete(sale.Id);
        }

        public void CheckProductSale(ProductModel product)
        {
            TimeSpan expiration = product.ExpirationDate - DateTime.Now.Date;
            if (expiration.TotalDays < 30)//Se vence dentro del próximo mes
            {
                new SaleBusiness().Insert(new SaleModel
                {
                    Available = true,
                    EndDate = product.ExpirationDate,
                    StartDate = DateTime.Now.Date,
                    ProductId = product.Id,
                    SalePrice = product.Price * 0.70m //Un 30% de descuento
                });
            }
        }

        public void DisableProductSale(int productId)
        {
            var sales = GetAll().Where(x => x.ProductId == productId).ToList();
            sales.ForEach(x => { x.Available = false; Update(x); });
        }
        #endregion
    }
}
