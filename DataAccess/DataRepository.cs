﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace DataAccess
{
    using SqlProviderServices = System.Data.Entity.SqlServer.SqlProviderServices;
    public class DataRepository<T> : IDataRepository<T> where T : class, ISoftDelete
    {
        #region Constructors
        public DataRepository()
        {
            this._context = new Demo3LayersEntities();
            table = _context.Set<T>();
        }
        public DataRepository(Demo3LayersEntities _context)
        {
            this._context = _context;
            table = _context.Set<T>();
        }
        #endregion

        #region Properties
        private Demo3LayersEntities _context;
        private DbSet<T> table;
        #endregion

        #region Methods
        public IEnumerable<T> GetAll()
        {
            return table.Where(x => !x.Deleted);
        }
        public T GetById(object id)
        {
            //No se utiliza .Where para mantener el genérico y no fijar un tipo de Id
            var entity = table.Find(id);
            return entity.Deleted ? null : entity;
        }
        public T Insert(T obj)
        {
            table.Add(obj);
            Save();
            return obj;
        }
        public void Update(T obj, object id)
        {
                //TODO quitar estos comentarios
                //table.Attach(obj);
                //_context.Entry(obj).State = EntityState.Modified;
                T existing = table.Find(id);
                _context.Entry(existing).CurrentValues.SetValues(obj);
                Save();
        }
        public void Delete(object id)
        {
                T existing = table.Find(id);
                existing.Deleted = true;
                Save();
        }
        private void Save()
        {
            _context.SaveChanges();
            //Save se implementa privado y en cada acción por la naturaleza de esta aplicación.
            //Ej: Inserta un producto e inmediatamente lo guardar en la BD.
            //Esto se puede desarrollar distinto, sin impactar en la BD hasta que la capa superior ejecute el método Save
        }
        #endregion
    }
}
