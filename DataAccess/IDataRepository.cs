﻿using System.Collections.Generic;

namespace DataAccess
{
    public interface IDataRepository<T> where T : ISoftDelete
    {
        IEnumerable<T> GetAll();
        T GetById(object id);
        T Insert(T obj);
        void Update(T obj, object id);
        void Delete(object id);
        //void Save();
        //Ver detalle en la clase de implementación
    }
}