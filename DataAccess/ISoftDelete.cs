﻿/// <summary>
/// Esta interfaz se crea para que el generador T4 (Demo3LayerModel.tt) pueda implementarla.
/// Lo que se busca es controlar, desde el repositorio genérico, el SoftDelete
/// </summary>
namespace DataAccess
{
    public interface ISoftDelete
    {
        bool Deleted { get; set; }
    }
}
