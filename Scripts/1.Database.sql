USE [master]
GO

CREATE DATABASE [Demo3Layers]
 
ALTER DATABASE [Demo3Layers] SET COMPATIBILITY_LEVEL = 130
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Demo3Layers].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO

ALTER DATABASE [Demo3Layers] SET ANSI_NULL_DEFAULT OFF 
GO

ALTER DATABASE [Demo3Layers] SET ANSI_NULLS OFF 
GO

ALTER DATABASE [Demo3Layers] SET ANSI_PADDING OFF 
GO

ALTER DATABASE [Demo3Layers] SET ANSI_WARNINGS OFF 
GO

ALTER DATABASE [Demo3Layers] SET ARITHABORT OFF 
GO

ALTER DATABASE [Demo3Layers] SET AUTO_CLOSE OFF 
GO

ALTER DATABASE [Demo3Layers] SET AUTO_SHRINK OFF 
GO

ALTER DATABASE [Demo3Layers] SET AUTO_UPDATE_STATISTICS ON 
GO

ALTER DATABASE [Demo3Layers] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO

ALTER DATABASE [Demo3Layers] SET CURSOR_DEFAULT  GLOBAL 
GO

ALTER DATABASE [Demo3Layers] SET CONCAT_NULL_YIELDS_NULL OFF 
GO

ALTER DATABASE [Demo3Layers] SET NUMERIC_ROUNDABORT OFF 
GO

ALTER DATABASE [Demo3Layers] SET QUOTED_IDENTIFIER OFF 
GO

ALTER DATABASE [Demo3Layers] SET RECURSIVE_TRIGGERS OFF 
GO

ALTER DATABASE [Demo3Layers] SET  DISABLE_BROKER 
GO

ALTER DATABASE [Demo3Layers] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO

ALTER DATABASE [Demo3Layers] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO

ALTER DATABASE [Demo3Layers] SET TRUSTWORTHY OFF 
GO

ALTER DATABASE [Demo3Layers] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO

ALTER DATABASE [Demo3Layers] SET PARAMETERIZATION SIMPLE 
GO

ALTER DATABASE [Demo3Layers] SET READ_COMMITTED_SNAPSHOT OFF 
GO

ALTER DATABASE [Demo3Layers] SET HONOR_BROKER_PRIORITY OFF 
GO

ALTER DATABASE [Demo3Layers] SET RECOVERY SIMPLE 
GO

ALTER DATABASE [Demo3Layers] SET  MULTI_USER 
GO

ALTER DATABASE [Demo3Layers] SET PAGE_VERIFY CHECKSUM  
GO

ALTER DATABASE [Demo3Layers] SET DB_CHAINING OFF 
GO

ALTER DATABASE [Demo3Layers] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO

ALTER DATABASE [Demo3Layers] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO

ALTER DATABASE [Demo3Layers] SET DELAYED_DURABILITY = DISABLED 
GO

ALTER DATABASE [Demo3Layers] SET QUERY_STORE = OFF
GO

USE [Demo3Layers]
GO

ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO

ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO

ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO

ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO

ALTER DATABASE [Demo3Layers] SET  READ_WRITE 
GO


