USE [Demo3Layers]
GO
SET IDENTITY_INSERT [dbo].[Category] ON 
GO
INSERT [dbo].[Category] ([Id], [Name], [Deleted]) VALUES (1, N'Lacteos', 0)
GO
INSERT [dbo].[Category] ([Id], [Name], [Deleted]) VALUES (2, N'Embutidos', 0)
GO
INSERT [dbo].[Category] ([Id], [Name], [Deleted]) VALUES (3, N'Panificados', 0)
GO
SET IDENTITY_INSERT [dbo].[Category] OFF
GO
SET IDENTITY_INSERT [dbo].[Product] ON 
GO
INSERT [dbo].[Product] ([Id], [Description], [ExpirationDate], [Stock], [Price], [CategoryId], [Deleted]) VALUES (2, N'Yoghurt entero frutilla', CAST(N'2020-04-26' AS Date), CAST(4.000 AS Numeric(18, 3)), 34.5000, 1, 0)
GO
INSERT [dbo].[Product] ([Id], [Description], [ExpirationDate], [Stock], [Price], [CategoryId], [Deleted]) VALUES (3, N'Salame picado grueso Canoli', CAST(N'2020-07-13' AS Date), CAST(15.000 AS Numeric(18, 3)), 102.0000, 2, 0)
GO
INSERT [dbo].[Product] ([Id], [Description], [ExpirationDate], [Stock], [Price], [CategoryId], [Deleted]) VALUES (4, N'Salame picado fino Canoli', CAST(N'2020-06-30' AS Date), CAST(10.000 AS Numeric(18, 3)), 95.0000, 2, 0)
GO
SET IDENTITY_INSERT [dbo].[Product] OFF
GO