﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Business;
using Business.Models;

namespace UI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Obtengo Categorías
            var categoryBus = new CategoryBusiness();
            List<CategoryModel> listCategory = categoryBus.GetAll().ToList();

            //Obtengo los Productos
            var productBus = new ProductBusiness();
            List<ProductModel> listProduct = productBus.GetAll().ToList();
            var algo = listProduct.First();//Busco el primer producto
            var algoCate = algo.Category;//Pido la categoría, y hace la composición de objetos.
            
            //Obtengo las Ofertas
            var saleBus = new SaleBusiness();
            List<SaleModel> listSales = saleBus.GetAll().ToList();

            //Inserto un registro de producto que se vence antes del mes y debería crearme automáticamente una oferta
            ProductModel newProduct = new ProductModel()
            {
                CategoryId = 3,
                Description = "Pan para panchos Fargo",
                ExpirationDate = new DateTime(2020, 04, 15),
                Price = 75.8m,
                Stock = 40
            };
            productBus.Insert(newProduct);

            //Rrefresco los productos y las ofertas para comprobar si impactaron los cambios
            listProduct = productBus.GetAll().ToList();
            listSales = saleBus.GetAll().ToList();
        }
    }
}
